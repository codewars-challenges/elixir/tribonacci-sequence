defmodule TribonacciSequence do

  @spec tribonacci({number, number, number}, non_neg_integer) :: [number]
  def tribonacci(signature, n) do
    List.flatten(trib(signature, n))
  end

  defp trib({_, _, _}, 0), do: []
  defp trib({a, _, _}, 1), do: [[], [a]]
  defp trib({a, b, _}, 2), do: [[], [a, b]]
  defp trib({a, b, c}, 3), do: [[], [a, b, c]]
  defp trib({a, b, c}, n) when n > 3 do
    [h, [t1, t2, t3]] = trib({a, b, c}, n - 1)
    [[h, t1], [t2, t3, t1 + t2 + t3]]
  end

end

trib = &TribonacciSequence.tribonacci/2
IO.inspect trib.({1, 1, 1}, 0), label: "The list is"
IO.inspect trib.({1, 1, 1}, 1), label: "The list is"
IO.inspect trib.({1, 1, 1}, 2), label: "The list is"
IO.inspect trib.({1, 1, 1}, 3), label: "The list is"
IO.inspect trib.({1, 1, 1}, 4), label: "The list is"
IO.inspect trib.({1, 1, 1}, 5), label: "The list is"
IO.inspect trib.({1, 1, 1}, 10), label: "The list is"
IO.inspect trib.({0, 0, 1}, 10), label: "The list is"
IO.inspect trib.({0, 1, 1}, 10), label: "The list is"
IO.inspect trib.({0.5, 0.5, 0.5}, 30), label: "The list is"